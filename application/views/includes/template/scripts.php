<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w"></script>
<!-- Scripts -->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery-1.11.0.min.js?ver=1"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->  
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery-2.1.0.min.js?ver=1"></script>
<!--<![endif]--> 
<script src="<?= base_url() ?>js/template/jquery.easing.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.tools.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.nav.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.scrollTo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.sticky.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.appear.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/responsiveslides.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/verge.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/custom.js"></script>
<script>  
    function initialize() { 
        var content = $("#google-map-id");
        var latlng = new google.maps.LatLng(content.data('lat'),content.data('lon'));
        var mapProp = {
            center: latlng,
            scrollwheel: false,
            zoom: content.data('zoom'),
            zoomControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            panControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('google-map-id'), mapProp);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: content.data('title'),
            icon:content.data('icon')
        });
        marker.setMap(map);
        var infowindow = new google.maps.InfoWindow({
            content: content.data('title')
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    }
     initialize();
</script>
<?php $this->load->view('predesign/gallery') ?>
<script>
function contactar(){
  $.post('<?= base_url('paginas/frontend/contacto') ?>',{telefono:$("#telefono").val(),titulo:$("#titulo").val(),nombre:$("#nombre").val(),mensaje:$("#mensaje").val(),email:$("#email").val()},function(data){
      $("#Conmessage").html(data);
      $("#Conmessage").show();
  });
  return false;
}
</script>
