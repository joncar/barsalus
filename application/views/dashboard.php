<h2>Registra el Menu Diario</h2>
<p>Precio del menu del dia: <input type="text" id="preciomenu" value="<?= $this->db->get('ajustes')->row()->precio_menu_dia ?>"></p>
<div>
    <?= $output ?>
</div>
<script>
    $(".list-group-item.active").each(function(){
        $(this).closest('.panel').addClass('panel-info').removeClass('panel-default');
    });
    $("input").on('click',function(e){
        e.stopPropagation();
    });
    $("#preciomenu").on('change',function(){
        $.post('<?= base_url('platos/admin/ajustes/update') ?>/1',{precio_menu_dia:$(this).val()},function(data){});
    });
    $(".list-group-item input[type='text']").on('change',function(e){
        var descripcion = $(this).val();
        var elemento = $(this).closest('.list-group-item');
        var id = elemento.data('id');
        $.post('<?= base_url('platos/admin/menu_dia/update') ?>/'+id,{descripcion:descripcion},function(data){$(".ajax_refresh_and_loading").trigger('click');});
    });
    
    $(".list-group-item").on('click',function(e){
        e.preventDefault();        
        var element = $(this);
        var id = element.attr('data-id');        
        var categoria = element.attr('data-categoria');
        var descripcion = element.find('input[type="text"]').val();
        if(element.hasClass('active')){            
            $.post('<?= base_url('platos/admin/menu_dia/delete') ?>/'+id,{},function(data){                
                element.removeClass('active');
                element.find('input').attr('disabled',true);
                element.attr('data-id',0);
                element.closest('.panel').addClass('panel-default').removeClass('panel-info');
                $(".ajax_refresh_and_loading").trigger('click');
            });
        }else{
            $.post('<?= base_url('platos/admin/menu_dia/insert') ?>/',{descripcion:descripcion,categoria:categoria},function(data){                
                element.addClass('active');
                element.find('input').attr('disabled',false);
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    element.attr('data-id',data.insert_primary_key);
                    element.closest('.panel').addClass('panel-info').removeClass('panel-default');
                }
                $(".ajax_refresh_and_loading").trigger('click');
            });            
        }        
    });
</script>
