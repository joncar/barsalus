<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie7" lang="es"><![endif]-->
<!--[if IE 8 ]><html class="ie8" lang="es"><![endif]-->
<!--[if IE 9 ]><html class="ie9" lang="es"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html lang="es"><!--<![endif]-->
    <head>
        <title><?= empty($title) ? 'Can Salus' : $title ?></title>
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- Seo Meta -->
        <meta name="description" content="">
        <meta name="keywords" content="">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/font-awesome.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/prettyPhoto.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/owl.carousel.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/owl.theme.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/animate.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/style.css" media="screen" />
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,300,200,100,500' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Titan+One" rel="stylesheet">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>img/favicon.ico">
        <?php if(isset($share) && $share): ?>
            <script>
                document.location.href="<?= site_url() ?>";
            </script>
        <?php else: $share = false; ?>            
        <?php endif ?>
    </head>
    <body>
        <?php if(!$share): ?>
            <?php $this->load->view('includes/template/header'); ?>
        <?php endif ?>
        <?php $this->load->view($view); ?>
        <?php if(!$share): ?>
            <?php $this->load->view('includes/template/footer'); ?>
            <?php $this->load->view('includes/template/scripts'); ?>
        <?php endif ?>
    </body>
</html>
