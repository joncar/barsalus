<!-- start header -->
<header class="intro  flex-items-xs-middle  parallax" 
        data-stellar-background-ratio="0.5"
        data-stellar-vertical-offset="300"
        data-stellar-offset-parent="true"
        style="background-image: url('<?= base_url() ?>img/bg/bg_5.jpg');">

    <div class="pattern" style="opacity: 0.5;"></div>

    <div class="container">
        <div class="intro__text">            
            <h1 class="intro__title">Noticias</h1>
        </div>
    </div>
</header>
<!-- end header -->
<main role="main">
    <!-- start section -->
    <section class="section">
        <div class="container">
            <div class="blog blog--style-1">
                <div class="blog__inner">
                    <div class="row  js-isotope" data-isotope-options='{ "itemSelector": ".js-isotope__item", "transitionDuration": "0.8s", "percentPosition": "true", "masonry": { "columnWidth": ".js-isotope__grid-sizer" }}'>
                        <div class="col-xs-12 col-md-6 col-lg-4  js-isotope__grid-sizer" style="min-height: 0;"></div>
                        <?php foreach($detail->result() as $d): ?>
                            <!-- start item -->
                            <div class="col-xs-12 col-md-6 col-lg-4  js-isotope__item">
                                <div class="blog__item  mx-auto">
                                    <figure>
                                        <img class="img-fluid" src="<?= $d->foto ?>" alt="demo" />
                                    </figure>

                                    <div class="blog__entry">
                                        <div class="blog__post-date">
                                            <span><?= date("d",strtotime($d->fecha)) ?></span>
                                            <span><?= date("M",strtotime($d->fecha)) ?><br /><?= date("Y",strtotime($d->fecha)) ?></span>
                                        </div>

                                        <h3 class="blog__entry__title">
                                            <a href="<?= $d->link ?>"><?= $d->titulo ?></a>
                                        </h3>

                                        <span class="blog__entry__meta"><?= $d->comentarios ?> Comentarios</span>

                                        <p>
                                            <?= substr(strip_tags($d->texto),0,80).'...' ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- end item -->
                        <?php endforeach ?>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
</main>