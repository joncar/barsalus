<header class="intro  flex-items-xs-middle  parallax" 
        data-stellar-background-ratio="0.5"
        data-stellar-vertical-offset="300"
        data-stellar-offset-parent="true"
        style="background-image: url('<?= base_url() ?>img/bg/bg_5.jpg');">

    <div class="pattern" style="opacity: 0.25;"></div>

    <div class="container">
        <div class="intro__text">            
            <h1 class="intro__title"><?= $detail->titulo ?></h1>
            <p class="intro__post-date"><?= date("M, d-Y",strtotime($detail->fecha)) ?> | <?= $comentarios->num_rows() ?>   Comentarios</p>
        </div>
    </div>
</header>
<!-- end header -->

<main role="main">
    <!-- start section -->
    <section class="section">
        <div class="container">
            <div class="blog single-content">
                <div class="row flex-items-md-center">
                    <div class="col-md-10">
                        <?= $detail->texto ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->

    <!-- start section -->
    <section class="section section--with-border" id="comentarios">
        <div class="container">
            <div class="blog__feedback">
                <div class="row flex-items-md-center">
                    <div class="col-md-10">
                        <h4 class="h2"><?= $comentarios->num_rows() ?> Comentarios</h4>

                        <ul class="comments-list">
                            <?php foreach($comentarios->result() as $c): ?>  
                            <li>
                                <!-- start table -->
                                <table>
                                    <tr class="comment__header">
                                        <td>
                                            <div class="hidden-xs-down">
                                                <img class="comment__author-avatar circled" class="img-fluid" src="<?= base_url() ?>img/users_photos/1.png" width="110" height="110" alt="demo" />
                                            </div>
                                        </td>

                                        <td width="100%">
                                            <div class="hidden-sm-up col-MB-20">
                                                <img class="comment__author-avatar circled" class="img-fluid" src="<?= base_url() ?>img/users_photos/1.png" width="110" height="110" alt="demo" />
                                            </div>

                                            <p class="comment__author"><strong><?= $c->autor ?></strong></p>

                                            <p class="comment__post-date"><?= date('d-m-Y',strtotime($c->fecha)) ?></p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td colspan="2">
                                            <div class="comment__text">
                                                <p>
                                                    <?= strip_tags($c->texto) ?>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <!-- end table -->                                
                            </li>
                            <?php endforeach ?>  
                            <?php if($comentarios->num_rows()==0): ?>
                              <li>               
                                Se el primero en comentar sobre este artículo
                              </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->

    <!-- start section -->
    <section class="section section--with-border">
        <div class="container">
            <div class="">
                <div class="row flex-items-md-center">
                    <div class="col-md-10">
                        <h4 class="h2">Envia un comentario</h4>
                        <?php if(!empty($_SESSION['mensaje'])){
                                echo $_SESSION['mensaje'];
                                unset($_SESSION['mensaje']);
                          }?>
                        <form action="<?= base_url('blog/frontend/comentarios') ?>" method="post">
                            <div class="row">
                                <div class="col-md">
                                    <label class="input-wrp">
                                        <input class="textfield" type="text" placeholder="Nombre" name="autor" />
                                    </label>
                                </div>

                                <div class="col-md">
                                    <label class="input-wrp">
                                        <input class="textfield" type="text" placeholder="E-mail" name="email" />
                                    </label>
                                </div>
                            </div>

                            <label class="input-wrp">
                                <textarea class="textfield" placeholder="Comentario" name="texto"></textarea>
                            </label>
                            <input type="hidden" name="blog_id" value="<?= $detail->id ?>">
                            <button class="custom-btn primary" type="submit" role="button">Enviar ahora</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
</main>