<section id="about" class="about section grey section-without-pb">
    <div class="visible-lg"  style="background: transparent url(http://hipo.tv/barsalus/img/food_logo_1.png) no-repeat; background-size:100%; color:#e75137; display: block; height: 220px; position: absolute; width: 220px; right: 50px; font-size: 40.2px; padding: 100.6px 33.3px;
; margin-top: 150px;">
        <span class="extrabold"><?= $this->db->get('ajustes')->row()->precio_menu_dia ?>€</span>
    </div>
    <div class="container-fluid">
        <div class="row" style="margin-left:0px; margin-right:0px;">
            <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                <h2><span class="extrabold">El Menú</span> d'Avui</h2>
                <div class="section-title-line">
                    <div class="section-title-icon">
                        <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                    </div>
                    <hr>
                </div>				
                <p>
                    Cada dia elaborem un menú diferent i de qualitat amb aliments variats. <br>
                    Trobaràs productes de mar, de muntanya i de l'horta. <br> Un munt de plats per a escollir.                    
                </p>	
            </div><!-- end .section-title -->
            <div class="col-md-6 side-image-left">		
                <div class="image-slider owl-carousel animated" data-animation="fadeInLeft" data-animation-delay="1000">
                    <div class="swiper-slide"><img src="<?= base_url() ?>img/about_image.jpg" alt=""></div>
                    <div class="swiper-slide"><img src="<?= base_url() ?>img/about_image.jpg" alt=""></div>
                </div><!-- end .image-slider -->
            </div>
            <div class="col-md-6">
                
                <div class="tab-set animated" data-animation="fadeInRight" data-animation-delay="1000">
                    <ul class="tabs-titles">
                        <li>Primers</li>
                        <li>SEGONS</li>
                        <li>POSTRES</li>
                        <li>SUPLEMENTS</li>
                    </ul>
                    <?php $menu = $this->db->get('menu_del_dia')->row(); ?>
                    <div class="tab-content">
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'primers'))->result() as $m): ?>
                            <p>
                                <?= $menu->primero ?>
                            </p>
                            <p><a href="https://www.facebook.com/dialog/share?app_id=560405997463344&display=popup&title=Mira el menu del dia de barsalus&description=<?= urlencode(strip_tags($menu->primero)) ?>&quote=<?= urlencode(strip_tags($menu->primero)) ?>&caption=Menu+del+dia&href=<?= base_url() ?>#about&redirect_uri=<?= site_url() ?>"><i class="fa fa-facebook-square"></i> Share</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                    <div class="tab-content">                        
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'segons'))->result() as $m): ?>
                            <p>
                                <?= $menu->segundo ?>
                            </p>
                            <p><a href="https://www.facebook.com/dialog/share?app_id=560405997463344&display=popup&title=Mira el menu del dia de barsalus&description=<?= urlencode(strip_tags($menu->primero)) ?>&quote=<?= urlencode(strip_tags($menu->segundo)) ?>&caption=Menu+del+dia&href=<?= base_url() ?>#about&redirect_uri=<?= site_url() ?>"><i class="fa fa-facebook-square"></i> Share</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                    <div class="tab-content">                        
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'postres'))->result() as $m): ?>
                            <p>
                                <?= $menu->postres ?>
                            </p>
                            <p><a href="https://www.facebook.com/dialog/share?app_id=560405997463344&display=popup&title=Mira el menu del dia de barsalus&description=<?= urlencode(strip_tags($menu->postres)) ?>&quote=<?= urlencode(strip_tags($menu->primero)) ?>&caption=Menu+del+dia&href=<?= base_url() ?>#about&redirect_uri=<?= site_url() ?>"><i class="fa fa-facebook-square"></i> Share</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                    <div class="tab-content">                        
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'suplements'))->result() as $m): ?>
                            <p>
                                <?= $menu->suplemento ?>
                            </p>
                            <p><a href="https://www.facebook.com/dialog/share?app_id=560405997463344&display=popup&title=Mira el menu del dia de barsalus&description=<?= urlencode(strip_tags($menu->suplemento)) ?>&quote=<?= urlencode(strip_tags($menu->primero)) ?>&caption=Menu+del+dia&href=<?= base_url() ?>#about&redirect_uri=<?= site_url() ?>"><i class="fa fa-facebook-square"></i> Share</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                </div><!-- end .tab-set -->                
            </div>
        </div><!-- end .row -->
    </div><!-- end .container -->
</section>
