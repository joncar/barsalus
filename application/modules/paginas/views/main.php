<?php if(!$share): ?>
<section id="welcome" class="welcome section">
    <div class="container">
        <div class="row">
            <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                <h2><span class="extrabold">Benvingut al </span> nostre Restaurant</h2>
                <div class="section-title-line animated" data-animation="fadeInUp" data-animation-delay="900">
                    <div class="section-title-icon">
                        <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                    </div>
                    <hr>
                </div>				
                <p>
                    T'obrim les portes de Can Salus, on trobaràs una cuina tradicional i de mercat. El nostre personal qualificat és la nostra garantia. Els sabors dels nostres plats et faran viure una experiència que recordaràs i que ens posiciona com a un dels millors restaurants de la zona. Gaudeix d'esmorzars, aperitius, dinars i sopars o simplement d'un café a la nostra àmplia terrassa climatitzada.
                </p>	
            </div><!-- end .section-title -->
            <div class="welcome-image text-center">
                <img src="<?= base_url() ?>img/food_plate.png" alt="">
            </div>
            <div class="col-md-6">
                <h3 class="without-margin"><span class="extrabold">Menjar casolà</span></h3>
                <h3>Menjar tradicional</h3>
                <p>
                    La dieta mediterrània és la base de la nostra cuina amb l'ús de gran varietat de productes del mar, de muntanya i de l’horta. Apostem per una cuina catalana actualitzada basada en productes de la terra i adaptada a les estacions de l'any. Podràs escollir entre el menú diari o la nostra variada carta amb plats elaborats, tapes calentes i fredes, entrepans, pizzes i plats combinats.	Si ho desitges també fem menjar per a emportar.
                </p>
            </div>			
            <div class="col-md-6">
                <h3 class="without-margin"><span class="extrabold">La nostra <span class="highlight">millor</span> oferta</span></h3>
                <h3>un munt de possibilitats </h3>
                <div class="col-md-6">
                    <ul class="arrow-list">
                        <li>Menú Diari </li>
                        <li>Menú Festius </li>
                        <li>Cuina de temporada </li>
                        <li>Carta y cuina de mercat</li>
                    </ul>					
                </div>
                <div class="col-md-6">
                    <ul class="arrow-list">
                        <li>Menú i espais Infatil</li>
                        <li>Tapes i racions</li>
                        <li>Entrepans i pizzes</li>
                        <li>Plats combinats</li>
                    </ul>					
                </div>				
            </div>			
        </div><!-- end .row -->
    </div><!-- end .container -->
</section>
<?php endif ?>
<section id="about" class="about section grey section-without-pb">
    <div class="visible-lg"  style="background: transparent url(<?= base_url() ?>img/food_logo_1.png) no-repeat; background-size:100%; color:#e75137; display: block; height: 220px; position: absolute; width: 220px; right: 50px; font-size: 40.2px; padding: 100.6px 33.3px;
; margin-top: 100px;">
        <span class="extrabold"><?= $this->db->get('ajustes')->row()->precio_menu_dia ?>€</span>
    </div>
    <div class="container-fluid">
        <div class="row" style="margin-left:0px; margin-right:0px;">
            <?php if(!$share): ?><!--Ocultar para facebook-->
                <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                    <h2><span class="extrabold">El Menú</span> d'Avui</h2>
                    <div class="section-title-line">
                        <div class="section-title-icon">
                            <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                        </div>
                        <hr>
                    </div>				
                    <p>
                        Cada dia elaborem un menú diferent i de qualitat amb aliments variats. <br>
                        Trobaràs productes de mar, de muntanya i de l'horta. <br> Un munt de plats per a escollir.                    
                    </p>
                    <div style="position:relative;" class="hidden-lg">
                        <img src="http://cansalus.cat/img/food_logo_1.png">
                        <span class="extrabold" style="position: absolute; top: 56%; color: rgb(231, 81, 55); font-size: 40.2px; left: 0px; width: 100%; text-align: center;"><?= $this->db->get('ajustes')->row()->precio_menu_dia ?>€</span> 
                   </div>
                </div>
            <?php endif ?>
            <div class="col-md-6 side-image-left">		
                <div class="image-slider owl-carousel animated" data-animation="fadeInLeft" data-animation-delay="1000">
                    <div class="swiper-slide"><img src="<?= base_url() ?>img/about_image.jpg" alt=""></div>
                    <div class="swiper-slide"><img src="<?= base_url() ?>img/about_image.jpg" alt=""></div>
                </div><!-- end .image-slider -->
            </div>
            <div class="col-md-6">
                
                <div class="tab-set animated" data-animation="fadeInRight" data-animation-delay="1000">
                    <ul class="tabs-titles">
                        <li>Primers</li>
                        <li>SEGONS</li>
                        <li>POSTRES</li>
                        <li>SUPLEMENTS</li>
                    </ul>
                    <?php $menu = $this->db->get('menu_del_dia')->row(); ?>
                    <div class="tab-content">
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'primers'))->result() as $m): ?>
                            <p>
                                <?= $menu->primero ?>
                            </p>
                            <p><a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('main/index/share') ?>"><i class="fa fa-facebook-square"></i> Compartir</a> 
                                <a href="<?= base_url('pdf') ?>" target="_new"style=" margin-left: 30px" ><i class="fa fa-file-pdf-o"></i> Descargar</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                    <div class="tab-content">                        
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'segons'))->result() as $m): ?>
                            <p>
                                <?= $menu->segundo ?>
                            </p>
                            <p><a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('main/index/share') ?>"><i class="fa fa-facebook-square"></i> Compartir</a> 
                                <a href="<?= base_url('pdf') ?>" target="_new"style=" margin-left: 30px" ><i class="fa fa-file-pdf-o"></i> Descargar</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                    <div class="tab-content">                        
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'postres'))->result() as $m): ?>
                            <p>
                                <?= $menu->postres ?>
                            </p>
                            <p><a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('main/index/share') ?>"><i class="fa fa-facebook-square"></i> Compartir</a> 
                                <a href="<?= base_url('pdf') ?>" target="_new"style=" margin-left: 30px" ><i class="fa fa-file-pdf-o"></i> Descargar</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                    <div class="tab-content">                        
                        <?php //foreach($this->db->get_where('menu_dia',array('categoria'=>'suplements'))->result() as $m): ?>
                            <p>
                                <?= $menu->suplemento ?>
                            </p>
                            <p><a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('main/index/share') ?>"><i class="fa fa-facebook-square"></i> Compartir</a> 
                                <a href="<?= base_url('pdf') ?>" target="_new"style=" margin-left: 30px" ><i class="fa fa-file-pdf-o"></i> Descargar</a></p>
                        <?php //endforeach ?>
                    </div><!-- end .tab-content -->
                </div><!-- end .tab-set -->                
            </div>
        </div><!-- end .row -->
    </div><!-- end .container -->
</section>

<?php if(!$share): ?>
<section id="special-gallery" class="special-gallery section section-without-pb">
    <div class="container">
        <div class="row" style="margin-left:0px; margin-right:0px;">
            <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                <h2><span class="extrabold">Les Nostres</span> Especialitats</h2>
                <div class="section-title-line">
                    <div class="section-title-icon">
                        <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                    </div>
                    <hr>
                </div>				
                <p>
                    A Can Salus estem especialitzats en l'elaboració de tapes tradicionals, carns a la brasa, peix fresc, arrossos, amanides, pasta... Troba aquí una petita mostra dels plats que et podem oferir per a gaudir en el nostre restaurant o per a emportar.
                </p>                
            </div><!-- end .section-title -->
        </div><!-- end .row -->
    </div><!-- end .container -->
    <div class="my-gallery special-gallery-container clearfix">   
        <?php $this->db->order_by('orden','ASC'); ?>
        <?php $this->db->select('platos.*, categorias_platos.nombre'); ?>
        <?php $this->db->join('categorias_platos','categorias_platos.id = platos.categorias_platos_id'); ?>
        <?php foreach($this->db->get_where('platos',array('especialidad'=>1))->result() as $c): ?>
            <?php $c->foto_especialidad = base_url('img/platos/'.$c->foto_especialidad); ?>
                <div class="special-gallery-item">
                    <figure class="special-gallery-flow-effect" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" >
                        <a class="box-picture" href="<?= $c->foto_especialidad ?>" itemprop="contentUrl" data-size="1023x849">
                            <img src="<?= $c->foto_especialidad ?>">
                            <div class="special-gallery-overlay">
                                <div class="special-gallery-item-price"><?= $c->precio ?><span>€</span></div>
                                <h2><?= $c->platos_nombre ?></h2>
                                <!-- <h3>with Potatoes</h3> -->
                                <br>
                                <span class="special-gallery-item-cat"><?= $c->nombre ?></span>
                            </div><!-- end .special-gallery-overlay -->
                        </a>
                    </figure><!-- end .special-gallery-flow-effect -->			
                </div><!-- end .special-gallery-item -->
        <?php endforeach ?>
    </div><!-- end .special-gallery-container -->
</section>		

<section id="big-menu" class="big-menu section">
    <img id="food-logo" src="<?= base_url() ?>img/food_logo.png" alt="">
    <div class="container">
        <div class="row" style="margin-left:0px; margin-right:0px;">
            <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                <h2><span class="extrabold">La Nostra </span>  Carta</h2>
                <div class="section-title-line">
                    <div class="section-title-icon">
                        <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                    </div>
                    <hr>
                </div>				
                <p>
                    Gaudeix d'una gran varietat en plats elaborats, tapes, plats combinats, pizzes i entrepans fets amb productes de qualitat i proximitat.  <br>Vine a descobrir els nostres millors plats en família, amics o empresa.  <br>T'esperem per dinar o per sopar.
                </p>	
            </div><!-- end .section-title -->
            <div class="special-tab">		
                <ul class="special-tabs-titles">
                    <?php foreach($carta->result() as $c): ?>
                        <li><div class="<?= $c->icono ?>"></div><span><?= $c->nombre ?></span></li>
                    <?php endforeach ?>
                </ul>
                <?php foreach($carta->result() as $p): ?>
                    <div class="special-tab-content">
                        <div class="row" style="margin-left:0px; margin-right:0px;">  
                            <?php $this->db->order_by('orden','ASC'); ?>
                            <?php foreach($this->db->get_where('platos',array('categorias_platos_id'=>$p->id))->result() as $c): ?>
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                    <div class="food-menu-item animated" data-animation="fadeInUp" data-animation-delay="1000">
                                        <div class="pull-left food-menu-item-img-outer">
                                            <img class="img-circle" src="<?= base_url('img/platos/'.$c->foto) ?>" alt="">
                                        </div>
                                        <div class="food-menu-desc">
                                            <h5><?= $c->platos_nombre ?></h5>
                                            <p><?= $c->descripcion ?></p>
                                            <div class="food-menu-details">
                                                <span class="food-menu-weight"><?= $c->gramaje ?></span> | <span class="food-menu-price"><?= $c->precio ?>€</span>
                                            </div>
                                        </div>
                                    </div><!-- end .food-menu-item -->
                                </div><!-- end .col -->
                            <?php endforeach ?>
                        </div>
                    </div><!-- end .special-tab-content -->	
                <?php endforeach ?>															
            </div><!-- end .special-tab -->			
        </div><!-- end .row -->
    </div><!-- end .container -->
</section>	

<section id="cooks" class="cooks section grey">
    <div class="container">
        <div class="row" style="margin-left:0px; margin-right:0px;">
            <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                <h2><span class="extrabold">Els Nostre</span> Personal</h2>
                <div class="section-title-line">
                    <div class="section-title-icon">
                        <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                    </div>
                    <hr>
                </div>				
                <p>
                    El nostre equip compta amb una gran experiència en el món de l'hosteleria.  <br>Et faran sentir com a casa amb el seu tracte amable i professional.
                </p>
            </div><!-- end .section-title -->
            <div class="col-md-3 col-sm-6">
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="1000">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span> Salus</h4>
                    <h6>Jo mateix</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="1800">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img1.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span> Pilar</h4>
                    <h6>La dona de la casa</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>
            <div class="col-md-3 col-sm-6" >
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="2600">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img2.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span>Salus </h4>
                    <h6>El fundador</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="3400">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img3.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span> Raque</h4>
                    <h6>La de les ulleres de la barra</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>
            <div class="col-md-3 col-sm-6"style=" margin-top:30px">
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="4200">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img4.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span> Lucia</h4>
                    <h6>La jove de la casa</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>
            <div class="col-md-3 col-sm-6" style=" margin-top:30px">
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="5000">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img5.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span> Carmen</h4>
                    <h6>La cuinera</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>
            <div class="col-md-3 col-sm-6" style=" margin-top:30px">
                <div class="cook-face text-center animated" data-animation="tada" data-animation-delay="5800">
                    <div class="cook-face-outer">
                        <img class="img-circle" src="<?= base_url() ?>img/cook_img6.jpg" alt="">
                    </div>
                    <h4><span class="extrabold"></span> Clari</h4>
                    <h6>La morena</h6>
                    <!--
<p>
                        Grenadier jackfish arowana temperate perch bonytongue seamoth sea devil longfin Alaska blackfish yellow weaver. Elephant fish great white shark.
                    </p>
 -->
                </div>
            </div>

        </div><!-- end .row -->
    </div><!-- end .container -->
</section>

    <section class="full-bg video-bg">
        <div class="full-bg-overlay black-pat text-center">
            <h2 class="animated" data-animation="fadeInUp" data-animation-delay="1000">Un cop d'ull</h2>
            <a data-rel="prettyPhoto" href="https://www.youtube.com/watch?time_continue=1&v=GiBfKFJeaqA" class="video-play animated" data-animation="fadeInDown" data-animation-delay="1200">Video Play</a>
        </div><!-- end .full-bg-overlay -->
    </section><!-- end .full-bg -->

    <section id="contact" class="contact section grey section-without-pb">
        <div class="container-fluid">
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                    <h2><span class="extrabold">Contacta</span> amb nosaltres</h2>
                    <div class="section-title-line">
                        <div class="section-title-icon">
                            <img src="<?= base_url() ?>img/shrimp_icon.png" alt="">
                        </div>
                        <hr>
                    </div>
                    <p>
                        El nostre horari és de 06:00h a 00:00h tots els dies de la setmana menys dilluns.
                        <br>Per a reserves truca al 938 042 132 o envia un missatge al whatsapp 680 470 660 o omple el formulari que trobaràs en aquesta web. <br>Ens trobaràs a 50 metres de l'Hospital d'Igualada.
                    </p>
                </div><!-- end .section-title -->
                <div class="col-md-6 side-image-right pull-right">
                    <img src="<?= base_url() ?>img/contact_image.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <ul class="contact-details">
                        <li class="contact-address">C/Bèlgica, 2. 08700 IGUALADA <br/> 50m de l'Hospital d'Igualada</li>
                        <li class="contact-tel"><a href="tel:938042132" style=" color: #575a5c; text-decoration: underline">938 042 132</a></li>
                        <li class="contact-w"><a href="tel:680470660" style=" color: #575a5c; text-decoration: underline">680 47 06 60</a></li>
                        <li class="contact-email"><a href="mailto:reservas@cansalus.cat" style=" color: #575a5c; text-decoration: underline">reservas@cansalus.cat</a></li>
                        <li class="contact-email"><a href="mailto:info@cansalus.cat" style=" color: #575a5c; text-decoration: underline">info@cansalus.cat</a></li>
                    </ul>
                </div>
            </div><!-- end .row -->
        </div><!-- end .container -->
    </section>

    <section id="contact-form" class="contact-form section">
        <div class="container">
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <div id='Conmessage' class="log"></div>
                <form class="form-horizontal" action="" role="form" onsubmit="return contactar()">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input tabindex="1" type="text" class="form-control" name="your_name" id="nombre" placeholder="Nom">
                        </div>
                        <div class="form-group">
                            <input tabindex="2" type="email" class="form-control" name="your_email" id="email" placeholder="mail">
                        </div>
                        <div class="form-group">
                            <input tabindex="3" type="tel" class="form-control" name="your_phone" id="telefono" placeholder="Telèfon">
                        </div>
                        <div class="form-group">
                            <input tabindex="4" type="text" class="form-control" name="your_subject" id="titulo" placeholder="Tema">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <textarea tabindex="5" class="form-control" name="your_message" id="mensaje" rows="8" placeholder="Missatge"></textarea>
                        </div>
                        <div class="button">
                            <input type="hidden" id="sendContac" name="sendContact" value="send" />
                            <button tabindex="6" type="submit" name="submit" id="submit">Enviar Missatge</button>
                        </div>
                    </div>
                </form><!-- end contact form -->
            </div><!-- end .row -->
        </div><!-- end .container -->
    </section>
    <section id="google-map">
        <div class="googlemap-container">
            <div id="google-map-id" class="google-map" data-lat="41.589569" data-lon="1.620255" data-zoom="17" data-icon="<?= base_url('img/pin.png') ?>" data-title="Barsalus"></div>
        </div><!-- end .googlemap-container -->
    </section>
    <!--
<section class="full-bg">
            <div class="full-bg-overlay blue-pat text-center">
                <div class="container">
                    <div class="row">
                        <div class="owl-carousel testimonials">
                            <div class="testimonial">
                                <div class="pull-left testimonial-face">
                                    <img class="img-circle" src="<?= base_url() ?>img/testimonial_face.jpg" alt="">
                                </div>
                                <div class="testimonial-content">
                                    The food was great and overall the experience was wonderful.
                                </div>						
                            </div><!~~ end .testimonial ~~>
                            <div class="testimonial">
                                <div class="pull-left testimonial-face">
                                    <img class="img-circle" src="<?= base_url() ?>img/testimonial_face.jpg" alt="">
                                </div>
                                <div class="testimonial-content">
                                    The food was great and overall the experience was wonderful.
                                </div>						
                            </div><!~~ end .testimonial ~~>
                            <div class="testimonial">
                                <div class="pull-left testimonial-face">
                                    <img class="img-circle" src="<?= base_url() ?>img/testimonial_face.jpg" alt="">
                                </div>
                                <div class="testimonial-content">
                                    The food was great and overall the experience was wonderful.
                                </div>						
                            </div><!~~ end .testimonial ~~>										
                        </div><!~~ end .testimonials ~~>			
                    </div><!~~ end .row ~~>
                </div><!~~ end .container ~~>
            </div><!~~ end .full-bg-overlay ~~>
        </section><!~~ end .full-bg ~~>
 -->
<?php endif ?>
