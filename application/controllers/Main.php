<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    public $theme = '';
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'ca_ES', 'Catalan_Spain', 'Catalan');        
    }
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        $blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');        
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }
    
    function get_carta(){
        $categorias_platos = new Bdsource();
        $categorias_platos->init('categorias_platos');
        foreach($this->categorias_platos->result() as $n=>$b){            
            $this->categorias_platos->row($n)->fondo = base_url('img/platos/'.$b->fondo);     
            $this->db->order_by('orden','ASC');
            $platos = $this->db->get_where('platos',array('categorias_platos_id'=>$b->id));
            foreach($platos->result() as $n=>$p){
                $platos->row($n)->foto = base_url('img/platos/'.$p->foto);      
                $platos->row($n)->foto_especialidad = base_url('img/platos/'.$p->foto_especialidad);      
            }
            $this->categorias_platos->row($n)->platos = $platos;
        }
    }

    public function index($share = false) {
        $this->get_entries();
        $this->get_carta();
        $this->loadView(array('view'=>'main','share'=>$share, 'blog'=>$this->blog,'carta'=>$this->categorias_platos));
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
        }

}
