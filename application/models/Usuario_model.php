<?php
class Usuario_model  extends grocery_crud_model_MySQL{
        function __construct() {
            parent::__construct();
            $this->filter = $this->table_name!='user'?'user_id':'id';
        }
        function get_list()
        {
            if($this->table_name === null)
                    return false;

            $select = $this->protect_identifiers("{$this->table_name}").".*";
            $additional_fields = array();  
            if(!empty($this->relation))
            {
                    foreach($this->relation as $relation)
                    {
                            list($field_name , $related_table , $related_field_title) = $relation;
                            $unique_join_name = $this->_unique_join_name($field_name);
                            $unique_field_name = $this->_unique_field_name($field_name);

                            if(strstr($related_field_title,'{'))
                            {
                                $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE(".$this->protect_identifiers($unique_join_name).".".$this->ESCAPE_CHAR, $this->ESCAPE_CHAR.", ''),'"),str_replace("'","\\'",$related_field_title))."') as ".$this->protect_identifiers($unique_field_name);
                            }
                            else
                            {
                                    if(!strstr($related_field_title,'.'))
                                    $select .= ', ' . $this->protect_identifiers($unique_join_name. '.'. $related_field_title).' AS '. $this->protect_identifiers($unique_field_name);
                                    else{
                                        $select .= ', ' . $this->protect_identifiers($related_field_title);
                                        $rel = explode('.',$related_field_title);
                                        $this->db->join($rel[0],"$rel[0].id = $this->table_name.$rel[0]_id");
                                    }
                            }

                            if($this->field_exists($related_field_title)){
                                $additional_fields[$this->table_name. '.'. $related_field_title] = $related_field_title;    			   
                            }
                    }
            }
            if(!empty($this->relation_n_n))
            {
                $select = $this->relation_n_n_queries($select);
            }                    
            $this->db->select($select, false);
            if($this->in_table($this->filter,$this->table_name))
            $this->db->where($this->filter,$_SESSION['user']);
            $results = $this->db->get($this->table_name)->result();
            for($i=0; $i<count($results); $i++){
                foreach($additional_fields as $alias=>$real_field){
                    $results[$i]->{$alias} = $results[$i]->{$real_field};
                }
            }
            return $results;
        }       
        
    function get_total_results()
    {
        //set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
        $select = $this->protect_identifiers("{$this->table_name}").".*";
        $additional_fields = array();  
        if(!empty($this->relation))
        {
                foreach($this->relation as $relation)
                {
                        list($field_name , $related_table , $related_field_title) = $relation;
                        $unique_join_name = $this->_unique_join_name($field_name);
                        $unique_field_name = $this->_unique_field_name($field_name);

                        if(strstr($related_field_title,'{'))
                        {
                            $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE(".$this->protect_identifiers($unique_join_name).".".$this->ESCAPE_CHAR, $this->ESCAPE_CHAR.", ''),'"),str_replace("'","\\'",$related_field_title))."') as ".$this->protect_identifiers($unique_field_name);
                        }
                        else
                        {
                                if(!strstr($related_field_title,'.'))
                                $select .= ', ' . $this->protect_identifiers($unique_join_name. '.'. $related_field_title).' AS '. $this->protect_identifiers($unique_field_name);
                                else{
                                    $select .= ', ' . $this->protect_identifiers($related_field_title);
                                    $rel = explode('.',$related_field_title);
                                    $this->db->join($rel[0],"$rel[0].id = $this->table_name.$rel[0]_id");
                                }
                        }

                        if($this->field_exists($related_field_title)){
                            $additional_fields[$this->table_name. '.'. $related_field_title] = $related_field_title;    			   
                        }
                }
        }
        if(!empty($this->relation_n_n))
        {
            $select = $this->relation_n_n_queries($select);
        }            
        if($this->in_table($this->filter,$this->table_name))
        $this->db->where($this->filter,$_SESSION['user']);
        $this->db->select($select, false);
        return $this->db->get($this->table_name)->num_rows();
    }
    
    function db_insert($post_array)
    {
        $post_array[$this->filter] = $_SESSION['user'];
        $insert = $this->db->insert($this->table_name,$post_array);                    
        return $insert?$this->db->insert_id():false;
    }

    function db_update($post_array, $primary_key_value)
    {
        $primary_key_field = $this->get_primary_key();            
        return $this->db->update($this->table_name,$post_array, array( $primary_key_field => $primary_key_value,$this->filter=>$_SESSION['user']));
    }
    
    function db_delete($primary_key_value)
    {
        $primary_key_field = $this->get_primary_key();

        if($primary_key_field === false)
                return false;

        return $this->db->delete($this->table_name,array( $primary_key_field => $primary_key_value,$this->filter=>$_SESSION['user']));            
    }
    
    function get_edit_values($primary_key_value)
    {
        $this->db->where($this->filter,$_SESSION['user']);
        $result = parent::get_edit_values($primary_key_value);        
        return $result;
    }
    
    function in_table($field,$table){        
        foreach($this->db->field_data($table) as $t){
            if($t->name==$field){
                return true;
            }
        }
        return false;
    }
}
